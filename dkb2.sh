#!/bin/bash

# 0. bring up mysql container by docker-compose up -d
# 1. fill drupal tables into mysql by 
#    drush site install during  docker build  3hdeng/dru7-fpm:t0 image 
# 2. commit change of mysql container to image 3hdeng/mysql-drusied:t0

mydb_cname="mydbx"
Mysql_user0="myuser0"
Mysql_pass0="mypass0"

#========= dkb1.sh ==============
cip=$(./dkb1.sh ${mydb_cname} ${Mysql_user0} ${Mysql_pass0})
echo $cip" from dkb1.sh"
#================================

fname="Dockerfile_dru7_fpm"
echo "Dockerfile fname= "$fname
#sed -i 's/172\.17\.0\.2/${cip}:8306/' $fname
sed -e 's/dummydbip/'${cip}'/' $fname > Dockerfile_tmp
sed -i  's/ENV Mysql_user0 user0/ENV Mysql_user0 '${Mysql_user0}'/' Dockerfile_tmp

sed -i  's/ENV Mysql_pass0 pass0/ENV Mysql_pass0 '${Mysql_pass0}'/' Dockerfile_tmp



docker build   	-f Dockerfile_tmp -t 3hdeng/dru7-fpm:t0 .

#============= docker commit  to save database change to image
#docker commit $mydb_cname 3hdeng/mysql-drusied:t0
#docker stop $mydb_cname && docker rm $mydb_cname




