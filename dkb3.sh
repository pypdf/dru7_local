#!/bin/bash

# 0. bring up mysql container by docker-compose up -d
# 1. fill drupal tables into mysql by 
#    drush site install during  docker build  3hdeng/dru7-fpm:t0 image 
# 2. commit change of mysql container to image 3hdeng/mysql-drusied:t0

mydb_cname="mydbx"
Mysql_user0="myuser0"
Mysql_pass0="mypass0"

#========= dkb1.sh ==============
./dkb1_test.sh ${mydb_cname} ${Mysql_user0} ${Mysql_pass0}
cip=$?
echo $cip
#================================

