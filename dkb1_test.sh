#!/bin/bash

# 0. bring up mysql container by docker-compose up -d
# 1. fill drupal tables into mysql by 
#    drush site install during  docker build  3hdeng/dru7-fpm:t0 image 
# 2. commit change of mysql container to image 3hdeng/mysql-drusied:t0



# stop local mysql service to avoid conflict
mylog="mylog.txt"
sudo service mysql stop >> $mylog 2>&1

export mydb_cname=$1 #"mydbxxx"
export Mysql_user0=$2 #"myuser0"
export Mysql_pass0=$3 #"mypass0"


myecho () {
 echo -e "$1\n" >> $mylog 
}


myecho "start mysql docker to write dru7 ini data"
myecho "$mydb_cname $Mysql_user0 $Mysql_pass0 exported"



#docker-compose  -f mysql_non_persistent.yml up  -d --no-recreate
#hide console stdio/stderr
docker-compose  -f mysql.yml up  -d --no-recreate >> $mylog 2>&1

myecho "sleeping 15s"
sleep 15

myecho "mysql container shd already started ..."


cip=172.10.0.2
myecho "mysql instance private ip= $cip"
echo $cip


