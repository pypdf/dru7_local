#!/bin/bash

# 0. bring up mysql container by docker-compose up -d
# 1. fill drupal tables into mysql by 
#    drush site install during  docker build  3hdeng/dru7-fpm:t0 image 
# 2. commit change of mysql container to image 3hdeng/mysql-drusied:t0


#========= dkb1.sh ==============
# stop local mysql service to avoid conflict
sudo service mysql stop

export Mysql_user0="myuser0"
export Mysql_pass0="mypass0"
export mydb_cname="mydbx"

echo "start mysql docker to write dru7 ini data"

docker-compose  -f mysql.yml up  -d --no-recreate
echo "sleeping 15s"
sleep 15
echo "mysqld container shd already started ..."

#docker exec $mydb_cname  sh -c 'ps aux|grep mysqld'

dkexec ()
{
	#echo "Positional parameter 1 in the function is $1."
	docker exec $mydb_cname sh -c $1
	local ret=$?
	#echo "The exit code of this function is $RETURN_VALUE."
	echo "$ret"
}

dkexec 'ps aux|grep mysqld'
echo $?

x="mysql -u$Mysql_user0 -p$Mysql_pass0 -e 'status' > /dev/null 2>&1"

RET=1
while [[ RET -ne 0 ]]; do
	echo "=> Waiting for confirmation of MySQL service startup"
	sleep 5
	dkexec $x
	RET=$?
done



cip=$(./getcip.sh $mydb_cname)
echo "mysql instance private ip= "$cip

#==============

fname="Dockerfile_dru7_fpm"
echo "Dockerfile fname= "$fname
#sed -i 's/172\.17\.0\.2/${cip}:8306/' $fname
sed -e 's/dummydbip/'${cip}'/' $fname > Dockerfile_tmp
sed -i  's/ENV Mysql_user0 user0/ENV Mysql_user0 '${Mysql_user0}'/' Dockerfile_tmp

sed -i  's/ENV Mysql_pass0 pass0/ENV Mysql_pass0 '${Mysql_pass0}'/' Dockerfile_tmp



docker build   	-f Dockerfile_tmp -t 3hdeng/dru7-fpm:t0 .

# docker commit  to save database change to image
docker commit $mydb_cname 3hdeng/mysql-drusied:t0

docker stop $mydb_cname && docker rm $mydb_cname




