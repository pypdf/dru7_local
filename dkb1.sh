#!/bin/bash

# 0. bring up mysql container by docker-compose up -d
# 1. fill drupal tables into mysql by 
#    drush site install during  docker build  3hdeng/dru7-fpm:t0 image 
# 2. commit change of mysql container to image 3hdeng/mysql-drusied:t0



# stop local mysql service to avoid conflict
mylog="mylog.txt"
sudo service mysql stop >> $mylog 2>&1

export mydb_cname=$1 #"mydbxxx"
export Mysql_user0=$2 #"myuser0"
export Mysql_pass0=$3 #"mypass0"


myecho () {
 echo -e "$1\n" >> $mylog
}


myecho "start mysql docker to write dru7 ini data"
myecho "$mydb_cname $Mysql_user0 $Mysql_pass0 exported"



#docker-compose  -f mysql_non_persistent.yml up  -d --no-recreate
docker-compose  -f mysql.yml up  -d --no-recreate >> $mylog 2>&1

myecho "sleeping 15s"
sleep 15

myecho "mysql container shd already started ..."

#docker exec $mydb_cname  sh -c 'ps aux|grep mysqld'

dkexec ()
{
	#echo "Positional parameter 1 in the function is $1."
	docker exec $mydb_cname sh -c $1
	local ret=$?
	#echo "The exit code of this function is $RETURN_VALUE."
	echo "$ret"
}

dkexec 'ps aux|grep mysqld' >> $mylog 2>&1


x="mysql -u$Mysql_user0 -p$Mysql_pass0 -e 'status'"

RET=1
while [[ RET -ne 0 ]]; do
	myecho "=> Waiting for confirmation of MySQL service startup"
	sleep 5
	dkexec $x > /dev/null 2>&1
	RET=$?
done



cip=$(./getcip.sh $mydb_cname)
myecho "mysql instance private ip= "$cip
echo $cip


